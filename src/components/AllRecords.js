import React, { Fragment, useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import RecordList from './RecordList'
import { Card, Row,Col,Button,ListGroup,Table,Form, Container,InputGroup } from 'react-bootstrap';

export default function AllRecords() {
	const userIsAdmin = localStorage.getItem('isAdmin')
	const { user ,setUser} = useContext(UserContext);
	const [records, setRecords] = useState([]);


	const fetchData = () => {
		fetch('https://git.heroku.com/safe-beach-52232.git/all',{
			method: 'GET',
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setRecords(data.map(record => {
			return (
				<RecordList key={record._id} recordProp={record} />	
			)
		}))	
		})
	}
	useEffect(() => {
		fetchData()
	}, []);

	 
	return(

		<InputGroup className="m-1" >
        
      
		 <Table  >
                <thead >
                  <th>
                   <Row >
                   <Col><th >ID</th></Col>
                    <Col><th >NAME</th></Col>
                    </Row>
                  </th>
                </thead>
                
        <tbody>
        <tr><td>
        	<Row style={{width:'1000px'}} >{records}</Row></td></tr>
            
        </tbody>
      </Table>
		</InputGroup>


		
		
	)
}