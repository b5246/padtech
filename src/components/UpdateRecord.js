import React, { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, InputGroup } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Form from 'react-bootstrap/Form';

export default function UpdateRecord() {
	
	const { user } = useContext(UserContext)
	const history = useNavigate()
	const { recordId } = useParams()

	const [fullName,setFullName]=useState('')
	const [major, setMajor] = useState('')
	const [dateHired,setDateHired]=useState('')

	useEffect(()=> {
		fetch(`https://git.heroku.com/safe-beach-52232.git/record/${recordId}`,{
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
		})
		.then(res => res.json())
		.then(data => {
			setFullName(data.fullName);
			setMajor(data.major)
			setDateHired(data.dateHired)
		})

	}, [recordId])

	function recordData(e){
		e.preventDefault()
		fetch(`https://git.heroku.com/safe-beach-52232.git/update/${recordId}`,{
			    method: 'PUT',
			    headers: {
			        'Content-Type': 'application/json',
			        Authorization: `Bearer ${localStorage.getItem('token')}`,
			        'Accept': 'application/json'
			    },
			    body: JSON.stringify({
			        fullName: fullName,
			        major: major,
			        dateHired: dateHired
			    })
		})
		.then(res => res.json())
		.then(data => {
			console.log('hello',data)
		    if(!data){
		    	Swal.fire({
		            title: 'Something went wrong',
		            icon: 'error'
		        });
		       
		    } else{
			    Swal.fire({
			    title: `Record is Updated`,
			    icon: "success"
				})
				window.location.replace("/dashboard")
		    }
		    
		})	
	}
  return (
    <Form xs={1} md={2} class="mt-3" onSubmit={(e)=>recordData(e)}>
    			<h1 className="mt-0">Update Record</h1>
    			<p className="mb-5">Update the details below.
    			</p>
    			<Row className="mb-3">
    							<Form.Group as={Col} className="mb-3" controlId="fullName">
    								<Form.Label>Name</Form.Label>
    								<Form.Control 
    								  	type="text" 
    								  	placeholder="enter full name here" 
    								  	value={fullName}
    								  	onChange={e=>setFullName(e.target.value)}
    								  	required
    								 />
    							</Form.Group>
    						</Row>
    						<Row>
    							<Form.Group as={Col} className="mb-3" controlId="major">
    							      <Form.Label>Major</Form.Label>
    							      <Form.Select 
    							      	type="text"
    							      	value={major}
    							      	placeholder="select major"
    								    onChange={e=>setMajor(e.target.value)}
    							  	    required>
    							        <option>English</option>
    							        <option>Biology</option>
    							        <option>Science</option>
    							        <option>MAPEH</option>
    							        <option>Mathematics</option>
    							      </Form.Select>
    							</Form.Group>
    							<Form.Group as={Col} className="mb-3" controlId="dateHired">
    							      <Form.Label>Date Hired</Form.Label>
    							      <Form.Control 
    							      	type="date"
    							      	placeholder="dd-mm-yyyy"
    							      	value={dateHired}
    								    onChange={e=>setDateHired(e.target.value)}
    							  	    required>
    							      </Form.Control>
    							</Form.Group>
    						</Row>

    		    <Form.Group  className="m-4">
    				<Row><Button variant="primary" type="submit" id="submitBtn">
    				 	UPDATE Record
    				</Button></Row>
    				
    			</Form.Group>
    				
    		  	
    		</Form>
  );
}
