import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import { Card, Row,Col,Button,ListGroup,Table, Container,Form } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import { Navigate, useNavigate } from 'react-router-dom'


export default function RecordList({recordProp}) {
        const [rec,setRec]= useState(false)
        const [usrId,setUsrId]=useState('')
        const navigate = useNavigate()

        function detail(e){
            setRec(true)
                
        }
        function close(){
            setRec(false)
            let theFormItself =  document.getElementById('theForm')
            theFormItself.style.display = 'none'
        }
        
        function updateRecord(){
            navigate(`/record/${recordProp._id}`)
        }

        function deleteRecord(){
            fetch(`https://git.heroku.com/safe-beach-52232.git/${recordProp._id}`,{
                    method: 'DELETE',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => res.json())
                .then(data=>{
                    if(data){
                            Swal.fire({
                            title: `A record is Deleted!`,
                            icon: "success"
                            })
                            window.location.replace("/dashboard")
                            
                    } else{
                            Swal.fire({
                                title: 'Something wrong',
                                icon: 'error',
                                text: 'Please try again.' 
                            });
                            
                    }
                    
                }) 
        }

        
        return (

            

           <Container fluid >
               <Table  variant="light" hover >
                       <tbody>
                         <tr>
                           <Row>
                           <Col  ><td>{recordProp._id}</td></Col>
                           <Col  ><td>{recordProp.fullName}</td></Col>
                            {
                                (rec===false)?

                                    <Col className="justify-content-end">
                                    <Row>
                                        <Button  variant="outline-primary" className="btn mb-1 " onClick={detail}>DETAILS</Button>
                                        <Button  variant="outline-danger" className="btn mb-1 " onClick={deleteRecord}>DELETE</Button>
                                        <Button  variant="outline-warning" className="btn mb-1 " onClick={updateRecord}>UPDATE</Button>
                                    </Row>
                            
                            </Col>

                                        
                            :
                                <Col className="justify-content-end">
                                <hr
                                        style={{
                                            color: 'gray'
                                        }}
                                    />
                                <Form id='theform' className="m-3 border" style={{width:'900px'}} >
                                          
                                          <Row  className="m-1">

                                          <Col>
                                                <Form.Label  column="sm" >
                                                  Name
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                <Form.Label className=" text-primary" column="sm">
                                                  {recordProp.fullName}
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                <Form.Label  column="sm" >
                                                  Major:
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                <Form.Label className=" text-primary" column="sm">
                                                  {recordProp.major}
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                <Form.Label column="sm" >
                                                  Date Hired 
                                                </Form.Label>
                                            </Col>
                                            <Col>
                                                  <Form.Label  className=" text-primary" column="sm" >
                                                 {String(recordProp.dateHired)}                       
                                                 </Form.Label>
                                            </Col>
                                          </Row>
                                          
                                          
                                        </Form>
                                    
                                <Row>
                                    <Button  variant="outline-danger" onClick={close} className="btn mb-1 " >Close</Button>
                                </Row>  
                                
                                </Col>
                            }
                            

                           
                           </Row>

                   
                         </tr>

               
                       </tbody>

                     </Table>
                     
                
           </Container>

           
        )
}

