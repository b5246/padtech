import React, {useState, useEffect, useContext} from 'react';
import {Form, Button,Row,Col} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function CreateProduct(){
	
	const { user, setUser } = useContext(UserContext)
	const history = useNavigate();
	const [isActive,setIsActive] = useState(false)

	// State hooks to store the values of the input fields
	const [fullName,setFullName]=useState('')
	const [major,setMajor]=useState('')
	const [dateHired, setDateHired] = useState('')

	function registerRecord(e) {
        e.preventDefault()
        fetch('https://git.heroku.com/safe-beach-52232.git/enroll', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                fullName: fullName,
                major: major,
                dateHired: dateHired
            })
        })
        .then(res => res.json())
        .then(data => {
            if(!data){
            	Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error'
                });
                setFullName('')
                setDateHired('')
                setMajor('')
            } else{
        	    Swal.fire({
        	    title: `Successfuly Registered!`,
        	    icon: "success"
        		})
        		window.location.replace("/dashboard")
            }
            
        })     
    }

	useEffect(()=>{
		if(fullName!== '' && major!==''){
			setIsActive(true)

		} else{
			setIsActive(false)
		}
	}, [fullName,major,dateHired] )
	
	return (
		// ( user.id !== null) ?
		//     <Navigate to="/" />
		// :
		<Form onSubmit={(e)=>registerRecord(e)}>
			<h1 className="mt-0">Register a Record</h1>
			<p className="mb-5">Please fill up the details below.
			</p>
			<Row className="mb-3">
				<Form.Group as={Col} className="mb-3" controlId="fullName">
					<Form.Label>Name</Form.Label>
					<Form.Control 
					  	type="text" 
					  	placeholder="enter full name here" 
					  	value={fullName}
					  	onChange={e=>setFullName(e.target.value)}
					  	required
					 />
				</Form.Group>
			</Row>
			<Row>
				<Form.Group as={Col} className="mb-3" controlId="major">
				      <Form.Label>Major</Form.Label>
				      <Form.Select 
				      	type="text"
				      	value={major}
				      	placeholder="select major"
					    onChange={e=>setMajor(e.target.value)}
				  	    required>
				        <option>English</option>
				        <option>Biology</option>
				        <option>Science</option>
				        <option>MAPEH</option>
				        <option>Mathematics</option>
				      </Form.Select>
				</Form.Group>
				<Form.Group as={Col} className="mb-3" controlId="dateHired">
				      <Form.Label>Date Hired</Form.Label>
				      <Form.Control 
				      	type="date"
				      	value={dateHired}
					    onChange={e=>setDateHired(e.target.value)}
				  	    required>
				      </Form.Control>
				</Form.Group>
			</Row>
			

		    <Form.Group as={Row} className="m-4">
			
			{ 
				isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
				  Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit
				</Button>
			}
			</Form.Group>
		  	
		</Form>
	)
}