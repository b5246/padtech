import React, { useState, useEffect, useContext } from 'react'
import { Form, Button,Col,Row } from 'react-bootstrap'
import { Navigate ,useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function Login() {
        const { user, setUser } = useContext(UserContext);
        const navigate = useNavigate()
        const [fullName, setFullName] = useState('');
        const [password, setPassword] = useState('');
        const [isActive, setIsActive] = useState(false);

        function authenticate(e) {
            e.preventDefault()
            fetch('https://git.heroku.com/safe-beach-52232.git/login',{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body:JSON.stringify({
                    fullName: fullName,
                    password: password
                })
            })
            .then(res=>res.json())
            .then(data=>{
                console.log('hello',data)
                if (!data) {
                    Swal.fire({
                        title: "Email/Password is incorrect! ",
                        icon: "error",
                        text: "Check your login details and try again."
                    }) 
                } else {
                    localStorage.setItem('token',data.access)
                    fetch('https://git.heroku.com/safe-beach-52232.git/details',{
                        method: 'GET',
                        headers: {
                            Authorization: `Bearer ${data.access}`
                        },
                        

                    })
                    .then(res=>res.json())
                    .then(result=>{
                        if(result){
                            Swal.fire({
                                title: "Login Successful",
                                icon: "success",
                            })
                            localStorage.setItem('id',result._id)
                            localStorage.setItem('name',result.fullName)
                            localStorage.setItem('isAdmin',result.isAdmin)
                            
                            setUser({
                                id: result._id,
                                isAdmin: result.isAdmin
                            })
                            navigate('/dashboard')
                        } else{
                            Swal.fire({
                                title: "Authentication Failed!",
                                icon: "error",
                                text: "Check your login details and try again."
                            })
                            
                        }
                        
                        
                    })

                }
            })
            setFullName('');
            setPassword('');
        }
        useEffect(() => {
            if(fullName !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [fullName, password]);
        

        const formStyle = { 
            borderRadius: '20px',
            background: 'light gray',
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: '15px',
            textAlign: "center"  
        }

        

    return (

        <div className="m-8">
            <Form   style={formStyle}  className="m-5 p-1 border border-info" onSubmit={(e) => authenticate(e)}>
                <h1 className="mt-5">Welcome to Awesome School</h1>
                <p className="mb-5">Please sign in. </p>
                <Form.Group as={Row}  className="m-1"  controlId="userFullName">
                    <Form.Label row lg={2} >Full Name</Form.Label>
                    <Col >
                        <Form.Control 
                            className="text-center mt-4 mb-4"
                            type="text" 
                            placeholder="enter your full name here" 
                            value={fullName}
                            onChange={(e) => setFullName(e.target.value)}
                            required
                        />
                    </Col>
                    
                </Form.Group>

                <Form.Group as={Row} className="m-1"  controlId="password">
                    <Form.Label row lg={2} >Password</Form.Label>
                    <Col >
                    <Form.Control 
                        className="text-center mt-4 mb-4"
                        type="text" 
                        placeholder=" enter your password here" 
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} className="m-1 g-2">
                    <Col fluid>
                        { isActive ? 
                            <Button  variant="primary" type="submit" id="submitBtn">
                                Submit
                            </Button>
                            : 
                            <Button  variant="danger" type="submit" id="submitBtn" disabled>
                                Submit
                            </Button>
                        }
                        </Col>
                </Form.Group>
                

            </Form>            
        </div>

        

    )
}
