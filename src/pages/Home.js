import React, {Fragment, useState, useEffect, useContext} from 'react';
import {Form, Button, Card ,Row,Col, Tab} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom'
import UserContext from '../UserContext';
import ListGroup from 'react-bootstrap/ListGroup';
import AllRecord from '../components/AllRecords'
import RegisterRecord from '../components/RegisterRecord'

import '../App.css'


function Home() {
	const usrName = localStorage.getItem('name')
  return (
  	<Fragment >
  		<Tab.Container className="mt-5 p-3 bg-light" id="list-group-tabs-example" defaultActiveKey="#records">
  		    	     <Card.Header className=" m-3 text-center text-info border  border-info font-weight-bolder g-1 m-2"  xs={12}>
                 <h3>Dashboard</h3>
                  </Card.Header>
                    <Row>
                            
          <Col sm={3}>
            <ListGroup>
              <Card.Header  className="text-light bg-info text-center mt-2 pt-3" >
                  <p>Welcome User!</p>
                  <p className="font-weight-bolder">{usrName}</p>
                  <a className="font-weight-bolder mb-3" href="/logout">LOG-OUT</a>
              </Card.Header>	
              <ListGroup.Item action href="#records">
                ALL RECORDS
              </ListGroup.Item>
              <ListGroup.Item action href="#enroll">
                NEW RECORD
              </ListGroup.Item>
  		        
  		      </ListGroup>
  		    </Col>
  		    <Col sm={9}>
  		      <Tab.Content>
    		        <Tab.Pane eventKey="#records">
                  <AllRecord/>
                </Tab.Pane>
                <Tab.Pane eventKey="#enroll">
                  <RegisterRecord/>
                </Tab.Pane>
              
  		      </Tab.Content>
  		    </Col>
  		  </Row>
  		</Tab.Container>
  	</Fragment>
    
  );
}

export default Home;