import React from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, Navlink } from 'react-router-dom'

function NotFound() {
	  const data={
	  	title: "404 - Page not found",
	  	content:"The page is unavaialble",
	  	destination: "/",
	  	label:"Back home"

	  }
	  return (
	    <Row>
	    	<Col className="p-5">
	    		<h1>{title}</h1>
	    		<p>{content}</p>
	    		<Link to={destination}>{label}</Link>
	    	</Col>
	    </Row>
	  )
}

export default NotFound
