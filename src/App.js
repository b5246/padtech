import { Fragment, useState,useEffect } from 'react'
import { Container } from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Routes} from 'react-router-dom'
import './App.css';
import {UserProvider} from './UserContext'
import Login from './pages/Login'
import Home from './pages/Home'
import Footer from './components/Footer'
import Navbar from './components/Navbar'
import Logout from './pages/Logout'
import Error from './pages/Error'
import UpdateRecord from './components/UpdateRecord'

function App() {

  const [user,setUser]= useState({
    id:localStorage.getItem('id'),
    isAdmin:localStorage.getItem('isAdmin'),
  })

  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=>{
    console.log('isAdmin:',user.isAdmin)
    console.log(localStorage)
  },[user])

  return (
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <Navbar/>
        <Container>
          <Routes>
            <Route path="/dashboard" element={<Home/>} />
            <Route path="/" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/record/:recordId" element={<UpdateRecord/>} />
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
        <Footer/>
      </Router>
    </UserProvider> 
  );
}

export default App;
